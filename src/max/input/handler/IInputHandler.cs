﻿using max.input.container;
using max.input.container.types;
using max.input.group;
using max.serialize;
using System;
using System.Collections.Generic;

namespace max.input.handler
{
    public interface IInputHandler : IMaxSerializable
    {
        Type InputType { get; }

        bool Active { get; set; }

        Dictionary<string, IInputContainer> InputContainers { get; }

        InputGroup ParentGroup { get; set; }

        /// <summary>
        /// Updates all active containers.
        /// </summary>
        void Update();

        /// <summary>
        /// Resets containers.
        /// </summary>
        void Reset();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID">ID Value</param>
        /// <returns>Input state</returns>
        float GetBaseInputState(string ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID">ID Value</param>
        /// <returns>Input state</returns>
        float GetInputState(string ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID">ID Value</param>
        /// <returns>True/False</returns>
        bool GetInputPressed(string ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID">ID Value</param>
        /// <returns>True/False</returns>
        bool GetInputHold(string ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID">ID Value</param>
        /// <returns>True/False</returns>
        bool GetInputReleased(string ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID">ID Value</param>
        /// <returns>True/False</returns>
        bool GetInputNone(string ID);

        /// <summary>
        /// Resets the IO state of the input handler.
        /// </summary>
        void ResetState();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID">Input ID</param>
        void ResetInput(string ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID">Input IDs</param>
        void ResetInput(string[] ID);
        /// <summary>
        /// Creates a basic input handler.
        /// </summary>
        /// <param name="ID">Input ID</param>
        /// <returns></returns>
        IInputBasicContainer CreateBasic(string ID);
        /// <summary>
        /// Creates a linked input handler.
        /// </summary>
        /// <param name="ID">Input ID</param>
        /// <returns></returns>
        IInputLinkedContainer CreateLinked(string ID);
        /// <summary>
        /// Creates a multi-press input handler.
        /// </summary>
        /// <param name="ID">Input ID</param>
        /// <returns></returns>
        IInputCombinedContainer CreateCombined(string ID);

        /// <summary>
        /// Returns the input container based on ID.
        /// </summary>
        /// <param name="ID">Input ID</param>
        /// <returns></returns>
        IInputContainer GetInputContainer(string ID);

    }
}
