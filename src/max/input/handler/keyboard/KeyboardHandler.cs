﻿using System;
using System.Collections.Generic;
using max.input.container;
using max.ds;
using max.input.key.keyboard;
using Microsoft.Xna.Framework.Input;
using max.input.container.types;
using max.input.container.keyboard;
using max.input.group;
using max.serialize;

namespace max.input.handler.keyboard
{
    /// <summary>
    /// This class handles all keyboard inputs.
    /// </summary>
    public class KeyboardHandler : IInputHandler
    {
        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public bool Serializable { get; set; } = true;

        const string SER_DEF = "definition";
        public InputGroup ParentGroup { get; set; }

        #region Properties
        public static KeyboardAllHandler.Action StdinEnterAction
        {
            get
            {
                return stdin.EnterAction;
            }
            set
            {
                stdin.EnterAction = value;
            }
        }

        public static KeyboardAllHandler.Action StdinEscAction
        {
            get
            {
                return stdin.EscAction;
            }
            set
            {
                stdin.EscAction = value;
            }
        }

        public static bool StandardInput { get; set; } = false;

        public Dictionary<string, IInputContainer> InputContainers
        {
            get
            {
                return new Dictionary<string, IInputContainer>(Definition);
            }
        }

        public bool Active { get; set; } = true;
        public static string KeyboardString { get
            {
                if (StandardInput)
                {
                    return stdin.KeyboardText;
                }
                return "";
            }
            set
            {
                stdin.KeyboardText = value;
            }
        }

        public static KeyboardAllHandler stdin = new KeyboardAllHandler();

        public KeyboardState CurrentState { get; private set; } = Keyboard.GetState();

        public KeyboardState PreviousState { get; private set; }

        public Type InputType { get
            {
                return _type;
            } }

        static Type _type = typeof(KeyInput);

        Dictionary<string, IInputContainer> Definition { get; set; } = new Dictionary<string, IInputContainer>();

        IInputContainer[] containers = new IInputContainer[0];

        #endregion

        #region constructor
        public KeyboardHandler(InputGroup ParentGroup)
        {
            this.ParentGroup = ParentGroup;
        }

        public KeyboardHandler()
        {

        }

        public KeyboardHandler(InputGroup ParentGroup, MaxSerializedObject Data)
        {
            this.ParentGroup = ParentGroup;
            Deserialize(Data);
        }
        #endregion

        #region Control methods

        public void Reset()
        {
            for (int i = 0; i < containers.Length; i++)
            {
                containers[i].Reset();
            }
        }

        public void Update()
        {
            if (!StandardInput)
            {
                UpdateKeyboardState();

                for(int i = 0; i < containers.Length; i++) {
                    containers[i].Update();
                }
            }
        }

        public void ResetState()
        {
            CurrentState = new KeyboardState();
            PreviousState = new KeyboardState();

            for (int i = 0; i < containers.Length; i++)
            {
                containers[i].Reset();
            }
        }

        public void CreateBasic(string v, object p)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Input Checking Methods

        public float GetInputState(string ID)
        {
            return Definition[ID].TimerState;
        }

        public float GetBaseInputState(string ID)
        {
            return Definition[ID].State;
        }

        public bool GetInputPressed(string ID)
        {
            if(GetInputState(ID) == InputController.STATE_PRESSED)
            {
                return true;
            }
            return false;
        }

        public bool GetInputHold(string ID)
        {
            if (GetInputState(ID) == InputController.STATE_HOLD)
            {
                return true;
            }
            return false;
        }

        public bool GetInputReleased(string ID)
        {
            if (GetInputState(ID) == InputController.STATE_RELEASED)
            {
                return true;
            }
            return false;
        }

        public bool GetInputNone(string ID)
        {
            if (GetInputState(ID) == InputController.STATE_NONE)
            {
                return true;
            }
            return false;
        }

        #endregion

        #region Keyboard Handler Exclusive Methods

        /// <summary>
        /// Returns the state of the specifed key. 
        /// Keep in mind that it's generally better to
        /// just get the SourceState from the input of a basic keyboard
        /// input, but you can use this in your own implementations
        /// of IInput if you really want.
        /// </summary>
        /// <param name="key">The key to check</param>
        /// <returns>The state.</returns>
        public float GetState(Keys key)
        {
            bool prevstate = PreviousState.IsKeyDown(key);
            bool state = CurrentState.IsKeyDown(key);

            if (state)
            {
                if (prevstate)
                {
                    return InputController.STATE_HOLD;
                }
                else
                {
                    return InputController.STATE_PRESSED;
                }
            }
            else
            {
                if (prevstate)
                {
                    return InputController.STATE_RELEASED;
                }
                else
                {
                    return InputController.STATE_NONE;
                }
            }
        }

        private void UpdateKeyboardState()
        {
            PreviousState = CurrentState;
            CurrentState = Keyboard.GetState();
        }

        public void ResetInput(string ID)
        {
            for(int i = 0; i < containers.Length; i++)
            {
                if (containers[i].Active)
                {
                    containers[i].Reset();
                }
            }
        }

        public void ResetInput(string[] ID)
        {
            for (int i = 0; i < containers.Length; i++)
            {
                if (containers[i].Active)
                {
                    containers[i].Reset();
                }
            }
        }

        public IInputBasicContainer CreateBasic(string ID)
        {
            BasicKeyboardInputContainer newContainer = new BasicKeyboardInputContainer(this);
            Definition.Add(ID, newContainer);
            UpdateDefinitionsArray();
            return newContainer;
        }

        public BasicKeyboardInputContainer CreateKeyboardBasic(String ID)
        {
            return CreateBasic(ID, 'A');
        }

        public BasicKeyboardInputContainer CreateBasic(string ID, char Key)
        {
            BasicKeyboardInputContainer newContainer = new BasicKeyboardInputContainer(this);
            Definition.Add(ID, newContainer);
            newContainer.SetInput(Key);
            UpdateDefinitionsArray();
            return newContainer;
        }

        public BasicKeyboardInputContainer CreateBasic(string ID, string Key)
        {
            BasicKeyboardInputContainer newContainer = new BasicKeyboardInputContainer(this);
            Definition.Add(ID, newContainer);
            newContainer.SetInput(Key);
            UpdateDefinitionsArray();
            return newContainer;
        }

        public BasicKeyboardInputContainer CreateBasic(string ID, Keys Key)
        {
            BasicKeyboardInputContainer newContainer = new BasicKeyboardInputContainer(this);
            Definition.Add(ID, newContainer);
            newContainer.SetInput(Key);
            UpdateDefinitionsArray();
            return newContainer;
        }

        public IInputLinkedContainer CreateLinked(string ID)
        {
            LinkedKeyboardInputContainer newContainer = new LinkedKeyboardInputContainer(this);
            Definition.Add(ID, newContainer);
            UpdateDefinitionsArray();
            return newContainer;
        }

        public LinkedKeyboardInputContainer CreateKeyboardLinked(string ID)
        {
            return (LinkedKeyboardInputContainer)CreateLinked(ID);
        }

        public IInputCombinedContainer CreateCombined(string ID)
        {
            CombinedKeyboardInputContainer newContainer = new CombinedKeyboardInputContainer(this);
            Definition.Add(ID, newContainer);
            UpdateDefinitionsArray();
            return newContainer;
        }

        public CombinedKeyboardInputContainer CreateKeyboardCombined(string ID)
        {
            return (CombinedKeyboardInputContainer)CreateCombined(ID);
        }

        public IInputContainer GetInputContainer(string ID)
        {
            if (Definition.ContainsKey(ID))
            {
                return Definition[ID];
            }
            return null;
        }

        public void RemoveInputContainer(string ID)
        {
            Definition.Remove(ID); 
            UpdateDefinitionsArray();
        }

        private void UpdateDefinitionsArray()
        {
            containers = new IInputContainer[Definition.Values.Count];
            Definition.Values.CopyTo(containers, 0);
        }

        #endregion

        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();

            MaxSerializedObject containers = MaxSerializedObject.Create(); 

            foreach(KeyValuePair<string,IInputContainer> contain in Definition)
            {
                containers.AddSerializedObject(contain.Key, contain.Value.Serialize());
            }
            Data.SetType(this);
            Data.AddSerializedObject(SER_DEF, containers);
            return Data;
        }

        public void Deserialize(MaxSerializedObject Data)
        {
            Definition.Clear();
            IInputContainer Container;
            if (Data.ContainsKey(SER_DEF))
            {
                MaxSerializedObject container = Data.GetSerializedObject(SER_DEF);

                foreach(KeyValuePair<string,object> obj in container.Data)
                {
                    Container = (IInputContainer)((MaxSerializedObject)obj.Value).Deserialize();
                    Container.ParentInputHandler = this;
                    Definition.Add(obj.Key, Container);
                }

            }
            UpdateDefinitionsArray();
        }

        public IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new KeyboardHandler();
            a.Deserialize(Data);
            return a;
        }
    }
}
