﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace max.input.handler.keyboard
{
    public class KeyboardAllHandler
    {

        /// <summary>Time for key repeat</summary>
        protected DateTime m_keyRepeatTime = DateTime.MinValue;

        protected DateTime m_keyRepeatSameTime = DateTime.MinValue;
        protected Keys m_keyRepeatSameKey;
        /// <summary>Character translation for ALL keyboard keys - contains an element for all values in the Keys enumeration</summary>
        protected List<String> m_xlateAllKeys;
        /// <summary>Character translation for ALL keyboard keys, shifted - contains an element for all values in the Keys enumeration</summary>
        protected List<String> m_xlateAllKeysShifted;
        /// current string
        public string KeyboardText {get; set;} = String.Empty;

        protected Keys[] m_currentState;
        protected Keys[] m_previousState;

        protected Keys LastKey;

        public delegate void Action();
        public Action EnterAction { get;  set; }
        public Action EscAction { get; set; }

        public static String TEXT_SPACEBAR = "Space";
        public static String TEXT_CLEARED = "Cleared";

        public KeyboardAllHandler()
        {
            Initialize();
        }

        protected void Initialize()
        {
            // OK - This Seems Very Heavyweight, And It Kinda Is.  But It Allows For
            // Easy Mapping Of A "Keys" Value To Any String, Which Makes It Possible
            // To Create Single Key Macros, Like Binding On Steroids, And Also Makes
            // It Easy To Handle Non-US Or Gaming Keyboards.

            // The Format Of The Lines Below Is A Comment Line That Identifies The Array
            // Indices, Which Correspond To Values In The "Keys" Enumeration, And Then
            // The Actual Values For Those Indices.
            m_xlateAllKeys = new List<String>(new[] {
			//   0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//  16   17   18   19   20   21   22   23   24   25   26   27   28   29   30   31	
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//  32 	 33   34   35   36   37   38   39   40   41   42   43   44   45   46   47
				" ", "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	48   49   50   51   52   53   54   55   56   57   58   59   60   61   63   63
				"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "" , "" , "" , "" , "" , "" ,
			//	64	 65	  66   67   68	 69	  70   71	72	 73	  74   75	76	 77	  78   79
				"" , "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",
			//	80   81   82   83   84   85   86   87   88   89   90   91   92   93   94   95
				"p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "" , "" , "" , "" , "" ,
			//	96   97   98   99   100  101  102  103  104  105  106  107  108  109  110  111
				"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "*", "+", ",", "-", ".", "/",
			//	112  113  114  115  116  117  118  119  120  121  122  123  124  125  126  127
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	128  129  130  131  132  133  134  135  136  137  138  139  140  141  142  143
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	144  145  146  147  148  149  150  151  152  153  154  155  156  157  159  159
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	160  161  162  163  164  165  166  167  168  169  170  171  172  173  174  175
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	176  177  178  179  180  181  182  183  184  185  186  187  188  189  190  191
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , ";", "=", ",", "-", ".", "/",
			//	192  193  194  195  196  197  198  199  200  201  202  203  204  205  206  207
				"", "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	208  209  210  211  212  213  214  215  216  217  218  219  220  221  222  223
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "[", "\\","]", "'", "8",
			//	224  225  226  227  228  229  230  231  232  233  234  235  236  237  238  239
				"" , "" ,"|",  "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	240  241  242  243  244  245  246  247  248  249  250  251  252  253  254  255
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
                });

            // Create Collection Of Characters (Strings) For All Possible SHIFTED Key Values...
            m_xlateAllKeysShifted = new List<String>(new[] {
			//   0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//  16   17   18   19   20   21   22   23   24   25   26   27   28   29   30   31	
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//  32 	 33   34   35   36   37   38   39   40   41   42   43   44   45   46   47
				" ", "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	48   49   50   51   52   53   54   55   56   57   58   59   60   61   63   63
				")", "!", "@", "#", "$", "%", "^", "&", "*", "(", "" , "" , "" , "" , "" , "" ,
			//	64	 65	  66   67   68	 69	  70   71	72	 73	  74   75	76	 77	  78   79
				"" , "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
			//	80   81   82   83   84   85   86   87   88   89   90   91   92   93   94   95
				"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "" , "" , "" , "" , "" ,
			//	96   97   98   99   100  101  102  103  104  105  106  107  108  109  110  111
				"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "*", "+", ",", "-", ".", "/",
			//	112  113  114  115  116  117  118  119  120  121  122  123  124  125  126  127
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	128  129  130  131  132  133  134  135  136  137  138  139  140  141  142  143
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	144  145  146  147  148  149  150  151  152  153  154  155  156  157  159  159
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	160  161  162  163  164  165  166  167  168  169  170  171  172  173  174  175
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	176  177  178  179  180  181  182  183  184  185  186  187  188  189  190  191
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , ":", "+", "<", "_", ">", "?",
			//	192  193  194  195  196  197  198  199  200  201  202  203  204  205  206  207
				"", "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	208  209  210  211  212  213  214  215  216  217  218  219  220  221  222  223
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "{", "|", "}","\"", "*",
			//	224  225  226  227  228  229  230  231  232  233  234  235  236  237  238  239
				"" , "" ,"\\", "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
			//	240  241  242  243  244  245  246  247  248  249  250  251  252  253  254  255
				"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,
            });
            return;
        }

        /// <summary>Process a keyboard key.  Assumes that the Keyboard property is not null!</summary>
        /// <param name="key">The key to process</param>
        protected virtual void ProcessKey(Keys key)
        {
            LastKey = key;
            // Space - Special Handling To Eliminate Leading Whitespace
            if ((key == Keys.Space) && (!String.IsNullOrWhiteSpace(KeyboardText)))
            {
                KeyboardText += " ";
                TTSRead(TEXT_SPACEBAR);
            }
            // Check input for Delete, clear string
            else if (key == Keys.Delete)
            {
                KeyboardText = String.Empty;
                TTSRead(TEXT_CLEARED);
            }
            // Check input for Backspace
            else if ((key == Keys.Back) && (!String.IsNullOrEmpty(KeyboardText)))
            {
                if (KeyboardText.Length > 0)
                {
                    KeyboardText = KeyboardText.Remove(KeyboardText.Length - 1, 1);
                    if (KeyboardText.Length > 0)
                    {
                        TTSRead(KeyboardText.ToCharArray()[KeyboardText.Length - 1].ToString());
                        SayEmptyKey(key);
                    }
                }
            }
            // Enter action
            else if ((key == Keys.Enter) && (!String.IsNullOrWhiteSpace(KeyboardText)))
            {
                EnterAction?.Invoke();
            }
            // ESC action
            else if ((key == Keys.Escape))
            {
                EscAction?.Invoke();
            }
            else if ((key == Keys.F1) && (!String.IsNullOrWhiteSpace(KeyboardText)))
            {
                TTSRead(KeyboardText);
            }
            // Any Other Key, Process As Character (Even If Not A Real Character Key)
            else
            {
                // Handle Shifted Chars
                if ((Keyboard.GetState().IsKeyDown(Keys.LeftShift)) ||
                        (Keyboard.GetState().IsKeyDown(Keys.RightShift)))
                {
                    KeyboardText += m_xlateAllKeysShifted[(int)key];
                    TTSRead(m_xlateAllKeysShifted[(int)key]);
                }
                else
                {
                    KeyboardText += m_xlateAllKeys[(int)key];
                    TTSRead(m_xlateAllKeys[(int)key]);
                }

                //certain keys have no text when pressed which should indicate to the player they were pressed.
                SayEmptyKey(key);
            }
            return;
        }

        private void SayEmptyKey(Keys key)
        {
            String say = null;
            if(key == Keys.Space)
            {
                say = TEXT_SPACEBAR;
            }

            if(say != null)
            {
                TTSRead(say);
            }
        }

        /// <summary>Check input from the provided keyboard interface</summary>
        protected virtual void CheckInput()
        {
            //InputManager	im = InputManager.Instance;
            m_currentState = Keyboard.GetState().GetPressedKeys();

            if (m_previousState == null)
            {
                m_previousState = m_currentState;
            }

            bool tester;
            List<Keys> newKeys = new List<Keys>();
            foreach (Keys k in m_currentState)
            {
                tester = false;
                foreach (Keys z in m_previousState)
                {
                    if (z.Equals(k))
                    {
                        tester = true;
                        break;
                    }
                }

                if (!tester)
                {
                    newKeys.Add(k);
                }
            }
            if (m_currentState.Length > 0)
            {
                newKeys.Remove(m_currentState[0]);
            }

            if (m_currentState.Length > 0)
            {
                DateTime testTime = m_keyRepeatTime;

                if (Keyboard.GetState().GetPressedKeys()[0] == m_keyRepeatSameKey)
                {
                    testTime = m_keyRepeatSameTime;
                }
                if (DateTime.Now > testTime)
                {
                    ProcessKey(Keyboard.GetState().GetPressedKeys()[0]);
                    m_keyRepeatTime = DateTime.Now.AddMilliseconds(100);
                    m_keyRepeatSameTime = DateTime.Now.AddMilliseconds(200);
                    m_keyRepeatSameKey = Keyboard.GetState().GetPressedKeys()[0];
                }
            }
            if (newKeys.Count > 0)
            {
                ProcessKey(newKeys[0]);
                m_keyRepeatTime = DateTime.Now.AddMilliseconds(250);
            }
            m_previousState = m_currentState;
            return;
        }
        public void Update()
        {
            CheckInput();
        }
   
        public void Clear()
        {
            KeyboardText = String.Empty;
        }

        public void SetDelay(int mill)
        {
            m_keyRepeatTime = DateTime.Now.AddMilliseconds(mill);
        }

        public Keys GetLastKey()
        {
            return LastKey;
        }


        public virtual void TTSRead(String text)
        {
            //maxseer overrides this so that text can be read while retaining the structure of the KeyboardAllHandler in max.input
        }

    }
}