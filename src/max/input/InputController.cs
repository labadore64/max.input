﻿using max.input.group;
using System.Collections.Generic;
using max.IO;
using static max.serialize.MaxSerializer;
using max.serialize;

namespace max.input
{
    /// <summary>
    /// <para>The input controller is the most abstract level of the input management system
    /// in max.input. It handles all input sources, such as keyboard, mouse, gamepad ect.</para>
    /// </summary>
    public class InputController
    {
        /// <summary>
        /// The directory in the game's AppData folder where the
        /// configs for the Input Controller are saved.
        /// </summary>
        public static string ConfigDirectory = "config\\input";

        /// <summary>
        /// The file extension of the input controller data.
        /// </summary>
        public static string ConfigExt = IOProperties.FileExtension;

        /// <summary>
        /// The format of how the configs are serialized. See enum Config for details.
        /// </summary>
        public static SerialFormat ConfigType = SerialFormat.YAML;

        public const float STATE_NONE = 1;
        public const float STATE_PRESSED = 2;
        public const float STATE_HOLD = 3;
        public const float STATE_RELEASED = 4;

        Dictionary<string,InputGroup> InputGroups = new Dictionary<string, InputGroup>();

        public Dictionary<string,InputGroup> Groups { get { return new Dictionary<string, InputGroup>(InputGroups); } }

        InputGroup[] _inputGroups;

        #region control methods

        public void Initialize()
        {
            
        }

        public void Update()
        {
            for(int i = 0; i < _inputGroups.Length; i++) {
                _inputGroups[i].Update();
            }
        }

        #endregion

        #region modify input groups

        public InputGroup AddGroup(string ID)
        {
            InputGroup group = new InputGroup(this);
            InputGroups.Add(ID,group);
            UpdateDefinitionsArray();
            return group;
        }

        public void RemoveGroup(string ID)
        {
            InputGroups.Remove(ID);
            UpdateDefinitionsArray();
        }

        public void ClearGroups()
        {
            InputGroups.Clear();
            UpdateDefinitionsArray();
        }

        public void EnableGroup(string ID)
        {
            if (InputGroups.ContainsKey(ID))
            {
                InputGroups[ID].Active = true;
            }
        }

        public void EnableOnlyGroup(string ID)
        {
            DisableAll();
            EnableGroup(ID);
        }

        public void DisableGroup(string ID)
        {
            if (InputGroups.ContainsKey(ID))
            {
                InputGroups[ID].Active = false;
            }
        }

        public void DisableAll()
        {
            for(int i = 0; i < _inputGroups.Length; i++)
            {
                _inputGroups[i].Active = false;
            }
        }

        private void UpdateDefinitionsArray()
        {
            _inputGroups = new InputGroup[InputGroups.Values.Count];
            InputGroups.Values.CopyTo(_inputGroups, 0);
        }


        #endregion

        #region get input states

        public bool GetInputPressed(string ID)
        {
            bool returner = false;
            for (int i = 0; i < _inputGroups.Length; i++)
            {
                returner = _inputGroups[i].GetInputPressed(ID);
                if (returner)
                {
                    return returner;
                }
            }

            return returner;
        }

        public bool GetInputReleased(string ID)
        {
            bool returner = false;
            for (int i = 0; i < _inputGroups.Length; i++)
            {
                returner = _inputGroups[i].GetInputReleased(ID);
                if (returner)
                {
                    return returner;
                }
            }

            return returner;
        }

        public bool GetInputHold(string ID)
        {
            bool returner = false;
            for (int i = 0; i < _inputGroups.Length; i++)
            {
                returner = _inputGroups[i].GetInputHold(ID);
                if (returner)
                {
                    return returner;
                }
            }

            return returner;
        }

        public bool GetInputNone(string ID)
        {
            bool returner = false;
            for (int i = 0; i < _inputGroups.Length; i++)
            {
                returner = _inputGroups[i].GetInputNone(ID);
                if (returner)
                {
                    return returner;
                }
            }

            return returner;
        }

        public bool GetBaseInputPressed(string ID)
        {
            float value;
            for (int i = 0; i < _inputGroups.Length; i++)
            {
                value = _inputGroups[i].GetBaseInputState(ID);
                if (value == STATE_PRESSED)
                {
                    return true;
                }
            }

            return false;
        }

        public bool GetBaseInputReleased(string ID)
        {
            float value;
            for (int i = 0; i < _inputGroups.Length; i++)
            {
                value = _inputGroups[i].GetBaseInputState(ID);
                if (value == STATE_RELEASED)
                {
                    return true;
                }
            }

            return false;
        }

        public bool GetBaseInputHold(string ID)
        {
            float value;
            for (int i = 0; i < _inputGroups.Length; i++)
            {
                value = _inputGroups[i].GetBaseInputState(ID);
                if (value == STATE_HOLD)
                {
                    return true;
                }
            }

            return false;
        }

        public bool GetBaseInputNone(string ID)
        {
            float value;
            for (int i = 0; i < _inputGroups.Length; i++)
            {
                value = _inputGroups[i].GetBaseInputState(ID);
                if (value == STATE_NONE)
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        public void ResetInput(string ID)
        {
            for (int i = 0; i < _inputGroups.Length; i++)
            {
                _inputGroups[i].ResetInput(ID);
            }
        }

        public void ResetInput(string[] IDs)
        {
            for (int i = 0; i < _inputGroups.Length; i++)
            {
                _inputGroups[i].ResetInput(IDs);
            }
        }

        public void Save(string GroupID)
        {
            if (InputGroups.ContainsKey(GroupID))
            {
                if (ConfigType == SerialFormat.XML)
                {
                    TextFileWriter.WriteFile("\\" + ConfigDirectory, GroupID, ConfigExt, SerializeXML(InputGroups[GroupID].Serialize(),"Group"));
                } else if (ConfigType == SerialFormat.JSON)
                {
                    TextFileWriter.WriteFile("\\" + ConfigDirectory, GroupID, ConfigExt, SerializeJSON(InputGroups[GroupID].Serialize()));
                }
                else if (ConfigType == SerialFormat.YAML)
                {
                    TextFileWriter.WriteFile("\\" + ConfigDirectory, GroupID, ConfigExt, SerializeYAML(InputGroups[GroupID].Serialize()));
                }
            }
        }

        public void SaveAll()
        {
            foreach(KeyValuePair<string,InputGroup> group in InputGroups)
            {
                Save(group.Key);
            }
        }

        public Dictionary<string,object> LoadAll()
        {
            Dictionary<string, object> Data = new Dictionary<string, object>();
            ClearGroups();
            //Get all the files from the directory
            UpdateDefinitionsArray();
            return Data;
        }

        public void LoadGroup(string GroupID)
        {
            MaxSerializedObject Data = null;
            
            if (ConfigType == SerialFormat.XML)
            {
                Data = DeserializeXML(TextFileReader.ReadFile("\\" + ConfigDirectory, GroupID, ConfigExt));
            }
            else if (ConfigType == SerialFormat.JSON)
            {
                Data = DeserializeJSON(TextFileReader.ReadFile("\\" + ConfigDirectory, GroupID, ConfigExt));
            }
            else if (ConfigType == SerialFormat.YAML)
            {
                Data = DeserializeYAML(TextFileReader.ReadFile("\\" + ConfigDirectory, GroupID, ConfigExt));
            }

            if (InputGroups.ContainsKey(GroupID))
            {
                InputGroups[GroupID] = new InputGroup(this, Data);
            }
            else
            {
                InputGroups.Add(GroupID, new InputGroup(this, Data));
            }

            UpdateDefinitionsArray();
        }

        #region Default Input Methods
        public bool TestUpPressed()
        {
            return GetInputPressed(InputTypes.Up);
        }

        public bool TestUpReleased()
        {
            return GetInputReleased(InputTypes.Up);
        }

        public bool TestUpHold()
        {
            return GetInputHold(InputTypes.Up);
        }

        public bool TestUpNone()
        {
            return GetInputNone(InputTypes.Up);
        }

        public bool TestLeftPressed()
        {
            return GetInputPressed(InputTypes.Left);
        }

        public bool TestLeftReleased()
        {
            return GetInputReleased(InputTypes.Left);
        }

        public bool TestLeftHold()
        {
            return GetInputHold(InputTypes.Left);
        }

        public bool TestLeftNone()
        {
            return GetInputNone(InputTypes.Left);
        }
        public bool TestDownPressed()
        {
            return GetInputPressed(InputTypes.Down);
        }

        public bool TestDownReleased()
        {
            return GetInputReleased(InputTypes.Down);
        }

        public bool TestDownHold()
        {
            return GetInputHold(InputTypes.Down);
        }

        public bool TestDownNone()
        {
            return GetInputNone(InputTypes.Down);
        }
        public bool TestRightPressed()
        {
            return GetInputPressed(InputTypes.Right);
        }

        public bool TestRightReleased()
        {
            return GetInputReleased(InputTypes.Right);
        }

        public bool TestRightHold()
        {
            return GetInputHold(InputTypes.Right);
        }

        public bool TestRightNone()
        {
            return GetInputNone(InputTypes.Right);
        }
        public bool TestSelectPressed()
        {
            return GetInputPressed(InputTypes.Select);
        }

        public bool TestSelectReleased()
        {
            return GetInputReleased(InputTypes.Select);
        }

        public bool TestSelectHold()
        {
            return GetInputHold(InputTypes.Select);
        }

        public bool TestSelectNone()
        {
            return GetInputHold(InputTypes.Select);
        }
        public bool TestCancelPressed()
        {
            return GetInputPressed(InputTypes.Cancel);
        }

        public bool TestCancelReleased()
        {
            return GetInputReleased(InputTypes.Cancel);
        }

        public bool TestCancelHold()
        {
            return GetInputHold(InputTypes.Cancel);
        }

        public bool TestCancelNone()
        {
            return GetInputNone(InputTypes.Cancel);
        }
        public bool TestAccessPressed(int Index)
        {
            return GetInputPressed(InputTypes.Access[Index]);
        }

        public bool TestAccessReleased(int Index)
        {
            return GetInputReleased(InputTypes.Access[Index]);
        }

        public bool TestAccessHold(int Index)
        {
            return GetInputHold(InputTypes.Access[Index]);
        }

        public bool TestAccessNone(int Index)
        {
            return GetInputNone(InputTypes.Access[Index]);
        }
        #endregion
    }
}
