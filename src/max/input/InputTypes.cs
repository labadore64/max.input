﻿
/// <summary>
/// A default set of input types used across max engine.
/// It would be good practice to use functions that have the same functionality
/// across your game use the same input as the defaults if possible so that users
/// can customize functionality easier - although this may not always be possible.
/// </summary>
namespace max.input
{
    public class InputTypes
    {
        public const string Up = "up";
        public const string Down = "down";
        public const string Left = "left";
        public const string Right = "right";
        public const string Select = "select";
        public const string Cancel = "cancel";

        public static readonly string[] Access = new string[8]{ "access1","access2","access3","access4","access5","access6","access7","access8" };
    }
}
