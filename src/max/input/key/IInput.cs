﻿using max.input.container;
using max.input.key.timer;
using max.serialize;
using System;

namespace max.input.key
{
    public interface IInput : IMaxSerializable
    {

        /// <summary>
        /// The input timer, for handling cooldown/tap to hold timing.
        /// </summary>
        InputTimer Timer { get; }

        /// <summary>
        /// The Type used for the Input Source.
        /// </summary>
        Type InputType { get; }

        /// <summary>
        /// The parent container that contains this input definition.
        /// </summary>
        IInputContainer Parent { get; }

        /// <summary>
        /// The source of the input, such as an XNA Key.
        /// </summary>
        object Source { get; set;}

        /// <summary>
        /// The returned value of the input state.
        /// </summary>
        float[] Value { get; }
        /// <summary>
        /// The source value of the input this frame.
        /// </summary>
        float[] SourceValue { get; }

        /// <summary>
        /// The source state of the input this frame.
        /// </summary>
        float SourceState { get; }

        /// <summary>
        /// The shorthand input state, usually used for boolean or 1 dimensional inputs.
        /// </summary>
        float State { get; }

        /// <summary>
        /// Updates the key state.
        /// </summary>
        void Update();

        /// <summary>
        /// Resets the key state.
        /// </summary>
        void Reset();
    }
}
