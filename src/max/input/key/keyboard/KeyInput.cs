﻿using max.input.container;
using max.input.handler.keyboard;
using max.input.key.timer;
using Microsoft.Xna.Framework.Input;
using System;
using max.serialize;

namespace max.input.key.keyboard
{
    public class KeyInput : IInput
    {
        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public bool Serializable { get; set; } = true;

        const string SER_KEY = "key";
        public InputTimer Timer { get; private set; }
        public float State
        {
            get
            {
                return KeyState;
            }
        }

        public object Source
        {
            get
            {
                return Key;
            }
            set
            {
                if (value.GetType() != _type)
                {
                    throw new ArgumentException("Source must be of type " + _type.FullName);
                }
            }
        }

        public float[] SourceValue
        {
            get; private set;
        }

        public float SourceState { get; private set; }

        Keys Key;

        public float[] Value {
            get
            {
                return new float[] { KeyState };
            }
        }

        public IInputContainer Parent { get; set; }

        public Type InputType
        {
            get
            {
                return _type;
            }
        }

        static Type _type = typeof(Keys);

        float KeyState;

        #region Constructors

        public KeyInput() { }

        public KeyInput(IInputContainer Parent)
        {
            Key = (Keys)char.ToUpper('A');
            this.Parent = Parent;
            SetTimers();
        }
        
        public KeyInput(IInputContainer Parent, MaxSerializedObject Data)
        {
            this.Parent = Parent;
            Deserialize(Data);
        }

        #endregion
        /// <summary>
        /// Create a new KeyData with a character value.
        /// </summary>
        /// <param name="Character">Character</param>
        public KeyInput(char Character, IInputContainer Parent)
        {
            Key = (Keys)char.ToUpper(Character);
            this.Parent = Parent;
            SetTimers();
        }

        /// <summary>
        /// Create a new KeyData with a character value.
        /// </summary>
        /// <param name="Character">Character</param>
        public KeyInput(string Character, IInputContainer Parent)
        {
            Key = (Keys)Character.ToUpper().ToCharArray()[0];
            this.Parent = Parent;
            SetTimers();
        }

        /// <summary>
        /// Create a new KeyData with XNA Keys values.
        /// </summary>
        /// <param name="Key">Key</param>
        public KeyInput(Keys Key, IInputContainer Parent)
        {
            this.Key = Key;
            this.Parent = Parent;
            SetTimers();
        }

        private void SetTimers()
        {
            Timer = new InputTimer(this);
        }

        public void Update()
        {
            KeyboardHandler Handler = (KeyboardHandler)Parent.ParentInputHandler;
            KeyState = Handler.GetState(Key);

            float value = 0;
            if (Handler.CurrentState.IsKeyDown(Key))
            {
                value = 1;
            }
            SourceValue = new float[] { value };
            SourceState = KeyState;

            Timer.Update();
        }

        public void Reset()
        {
            KeyState = InputController.STATE_NONE;

            float value = InputController.STATE_NONE;
            SourceValue = new float[] { value };
            SourceState = KeyState;
            Timer.Reset();
        }

        public override bool Equals(object obj)
        {
            if(obj.GetType() != typeof(KeyInput))
            {
                return false;
            }

            KeyInput other = (KeyInput)obj;
            if(other.Key == Key)
            {
                return true;
            }

            return false;

        }

        public override int GetHashCode() =>
           new { Key, State, Value, SourceValue, SourceState, Timer, Parent}.GetHashCode();

        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            Data.SetType(this);
            Data.AddKey(SER_KEY, Key);
            if (Timer.CooldownTimer.Serializable || Timer.DoubleHoldTimer.Serializable)
            {
                Data.AddSerializedObject(InputTimer.SerializeID, Timer);
            }
            return Data;
        }

        public void Deserialize(MaxSerializedObject Data)
        {
            Key = Data.GetKey(SER_KEY);
            if (Data.ContainsKey(InputTimer.SerializeID))
            {
                Timer = new InputTimer(this, Data);
            }
            else
            {
                Timer = new InputTimer(this);
            }
        }

        public IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new KeyInput();
            a.Deserialize(Data);
            return a;
        }
    }
}
