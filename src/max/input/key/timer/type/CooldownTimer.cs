﻿
using max.serialize;
using max.time.timer;
using System;
using System.Collections.Generic;

namespace max.input.key.timer.type
{
    public class CooldownTimer : ITimerType
    {
        #region Constants

        public const string SerializeID = "Cooldown";
        const string SER_USESTOPWATCH = "useStopwatch";
        const string SER_ENDFRAME = "frame";
        const string SER_ENDTIME = "time";

        #endregion

        #region Properties

        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public bool Serializable
        {
            get
            {
                return FrameEnd != -1 && StopwatchTicks != 0;
            }
            set { }
        }

        public bool Active { get; set; }
        Timer Timer = new Timer();

        public InputTimer Parent { get; private set; }

        public float State { get; private set; }

        #endregion

        #region Constructor
        public CooldownTimer()
        {

        }

        public CooldownTimer(InputTimer timer)
        {
            Parent = timer;
        }

        public CooldownTimer(InputTimer timer, MaxSerializedObject Data)
        {
            Parent = timer;
            Deserialize(Data);
        }

        #endregion

        int _timesPressed = 0;
        bool _useStopwatch;

        public long StopwatchTime { 
            get
            {
                return (long) Math.Round(StopwatchTicks * .00001);
            }
            set
            {
                StopwatchTicks = value * 100000;
            }
        }

        public long StopwatchTicks { get; set; }

        public int FrameEnd { get; set; }
        public bool UseStopwatch
        {
            get
            {
                return _useStopwatch;
            }
            set
            {
                _useStopwatch = value;
                //if switching, you should reset the timer.
                if (value)
                {
                    ResetTimer();
                    Timer.StopwatchTimer.Reset();
                } else
                {
                    ResetTimer();
                    Timer.FrameTimer.Reset();
                }
            }
        }

        internal float Update()
        {
            State = Parent.State;
            if (Active)
            {
                if (!_useStopwatch)
                {
                    Timer.FrameTimer.Update();
                }
                DoCooldown();
                EndTimer();
            }

            return State;
        }

        private void DoCooldown()
        {
            if (State == InputController.STATE_PRESSED)
            {
                _timesPressed++;
                if(_timesPressed == 1)
                {
                    StartTimer();
                }
            }

            if (_timesPressed > 1)
            {
                State = InputController.STATE_NONE;
            }
        }

        private void StartTimer()
        {
            if (_useStopwatch)
            {
                Timer.StopwatchTimer.Start();
            } else
            {
                Timer.FrameTimer.Start();
            }
        }

        private void EndTimer()
        {
            if (_useStopwatch)
            {
                if(Timer.StopwatchTimer.CheckTime(StopwatchTicks,0)){
                    ResetTimer();
                    Timer.StopwatchTimer.Reset();
                }
            } else
            {
                if (Timer.FrameTimer.CheckFrame(FrameEnd, -1))
                {
                    ResetTimer();
                    Timer.FrameTimer.Reset();
                }
            }
        }

        private void ResetTimer()
        {
            _timesPressed = 0;
        }

        public void Reset()
        {
            ResetTimer();
            Timer.StopwatchTimer.Reset();
            Timer.FrameTimer.Reset();
        }

        #region Serialize
        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            if (FrameEnd != 0)
            {
                Data.AddInt(SER_ENDFRAME, FrameEnd);
            }
            if (StopwatchTime != 0)
            {
                Data.AddLong(SER_ENDTIME, StopwatchTime);
            }
            if (UseStopwatch)
            {
                Data.AddBoolean(SER_USESTOPWATCH, UseStopwatch);
            }
            return Data;
        }

        public void Deserialize(MaxSerializedObject Data)
        {
            FrameEnd = 0;
            StopwatchTicks = 0;

            if (Data.ContainsKey(SerializeID))
            {
                Data = Data.GetSerializedObject(SerializeID);
            }
            FrameEnd = Data.GetInt(SER_ENDFRAME);
            StopwatchTime = Data.GetLong(SER_ENDTIME);
            UseStopwatch = Data.GetBoolean(SER_USESTOPWATCH);

            Active = true;

        }
        public IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new CooldownTimer();
            a.Deserialize(Data);
            return a;
        }

        #endregion
    }
}
