﻿using max.serialize;

namespace max.input.key.timer.type
{
    interface ITimerType : IMaxSerializable
    { 
        bool Active { get; set; }
        InputTimer Parent { get; }

        float State { get; }
        bool UseStopwatch { get; }

        void Reset();

    }
}
