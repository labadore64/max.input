﻿
using System.Collections.Generic;
using max.input.container;
using max.input.key.timer.type;
using max.serialize;

namespace max.input.key.timer
{
    public class InputTimer : IMaxSerializable
    {

        #region Properties
        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public bool Serializable { get; set; } = true;

        public const string SerializeID = "Timer";
        /// <summary>
        /// Whether or not the input timer is active. If inactive, it doesn't update.
        /// When you set the timer active or inactive, it will reset its state.
        /// </summary>
        public bool Active { get
            {
                return _active;
            }
            set
            {
                _active = value;
                Reset();
            }
        }

        bool _active;

        /// <summary>
        /// The cooldown timer. If active, it will ignore repeated inputs
        /// within a certain timescale or number of frames.
        /// </summary>
        public CooldownTimer CooldownTimer { get; private set; }

        /// <summary>
        /// The Tap to hold timer. If active, double tapping in quick succession
        /// within the time limit or number of frames, will set the input to the
        /// hold state until pressed again.
        /// </summary>
        public DoubleHoldTimer DoubleHoldTimer { get; private set; }

        /// <summary>
        /// The parent input to reference.
        /// </summary>
        public IInput ParentInput { get; private set; }

        /// <summary>
        /// The parent container to reference.
        /// </summary>
        public IInputContainer ParentContainer { get; private set; }

        /// <summary>
        /// The end state of the inputs after calculating the timers.
        /// </summary>
        public float State { get; private set; }

        #endregion

        #region Constructors

        public InputTimer()
        {

        }

        public InputTimer(IInput parent)
        {
            ParentInput = parent;
            InitializeTimers();
        }

        public InputTimer(IInputContainer parent)
        {
            ParentContainer= parent;
            InitializeTimers();
        }

        public InputTimer(IInput parent, MaxSerializedObject Data)
        {
            ParentInput = parent;
            Deserialize(Data);
        }

        public InputTimer(IInputContainer parent, MaxSerializedObject Data)
        {
            ParentContainer = parent;
            Deserialize(Data);
        }

        #endregion


        private void InitializeTimers()
        {
            CooldownTimer = new CooldownTimer(this);
            DoubleHoldTimer = new DoubleHoldTimer(this);
        }

        public void Update()
        {
            if (_active)
            {
                State = InputController.STATE_NONE;
                if (ParentInput != null)
                {
                    State = ParentInput.State;
                }
                else if (ParentContainer != null)
                {
                    State = ParentContainer.State;
                }
                State = DoubleHoldTimer.Update();
                State = CooldownTimer.Update();
            }
        }

        public void Reset()
        {
            DoubleHoldTimer.Reset();
            CooldownTimer.Reset();
        }

        /// <summary>
        /// Sets the cooldown timer based on frames.
        /// </summary>
        /// <param name="Frames">Number of update frames for cooldown</param>
        public void SetCooldownTimer(int Frames)
        {
            Active = true;
            CooldownTimer.Active = true;
            CooldownTimer.UseStopwatch = false;
            CooldownTimer.FrameEnd = Frames;
        }

        /// <summary>
        /// Sets the cooldown timer based on milliseconds.
        /// </summary>
        /// <param name="Milliseconds">Number of milliseconds for cooldown</param>
        public void SetCooldownTimer(long Milliseconds)
        {
            Active = true;
            CooldownTimer.Active = true;
            CooldownTimer.UseStopwatch = true;
            CooldownTimer.StopwatchTime = Milliseconds;
        }

        /// <summary>
        /// Sets the double tap to hold timer based on frames.
        /// </summary>
        /// <param name="Frames">Number of update frames for double tap to hold</param>
        public void SetDoubleTapTimer(int Frames)
        {
            Active = true;
            DoubleHoldTimer.Active = true;
            DoubleHoldTimer.UseStopwatch = false;
            DoubleHoldTimer.FrameEnd = Frames;
        }

        /// <summary>
        /// Sets the double tap to hold timer based on milliseconds.
        /// </summary>
        /// <param name="Milliseconds">Number of milliseconds for double tap to hold</param>
        public void SetDoubleTapTimer(long Milliseconds)
        {
            Active = true;
            DoubleHoldTimer.Active = true;
            DoubleHoldTimer.UseStopwatch = true;
            DoubleHoldTimer.StopwatchTime = Milliseconds;
        }

        #region Serialize

        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            if (CooldownTimer.Serializable)
            {
                Data.AddSerializedObject(CooldownTimer.SerializeID, CooldownTimer);
            }
            if (DoubleHoldTimer.Serializable)
            {
                Data.AddSerializedObject(DoubleHoldTimer.SerializeID, DoubleHoldTimer);
            }
            return Data;
        }

        public void Deserialize(MaxSerializedObject Data)
        {
            if (Data.ContainsKey(SerializeID))
            {
                Data = Data.GetSerializedObject(SerializeID);
            }
            CooldownTimer = new CooldownTimer(this, Data);

            DoubleHoldTimer = new DoubleHoldTimer(this, Data);
            Active = true;
        }

        public IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new InputTimer();
            a.Deserialize(Data);
            return a;
        }

        #endregion
    }
}
