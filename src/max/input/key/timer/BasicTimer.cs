﻿using max.serialize;
using max.time.timer;
using System;
using System.Collections.Generic;

namespace max.input.key.timer
{
    public class BasicTimer : IMaxSerializable
    {
        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public bool Serializable { get; set; } = true;

        public const string SerializeID = "basicTimer";

        const string SER_USESTOPWATCH = "useStopwatch";
        const string SER_STARTFRAME = "startFrame";
        const string SER_ENDFRAME = "endFrame";
        const string SER_STARTTIME = "startTime";
        const string SER_ENDTIME = "endTime";
        public bool Active { get { return Timer.Active; } set { Timer.Active = value; } }

        public int StartFrame { get; set; } = -1;
        public int EndFrame { get; set; } = -1;

        public bool IsSet {  get
            {
                return UseStopwatch ? !(StartMilliseconds == 0 && EndMilliseconds == 0) : !(StartFrame == -1 && EndFrame == -1);
            } }

        public long StartMilliseconds
        {
            get
            {
                return (long)Math.Round(_startTime * .00001);
            }
            set
            {
                _startTime = value * 100000;
            }
        }

        long _startTime;
        public long EndMilliseconds
        {
            get
            {
                return (long)Math.Round(_endTime * .00001);
            }
            set
            {
                _endTime = value * 100000;
            }
        }
        long _endTime;

        public float State { get; private set; }

        public bool UseStopwatch { get { return Timer.UseStopwatch; } set { Timer.UseStopwatch = value; } }

        Timer Timer = new Timer();

        public BasicTimer()
        {

        }

        public BasicTimer(Dictionary<string,object> Data)
        {
            Deserialize(new MaxSerializedObject(Data));
        }

        public BasicTimer(MaxSerializedObject Data)
        {
            Deserialize(Data);
        }

        public BasicTimer(object Data)
        {
            Deserialize(new MaxSerializedObject(Data));
        }

        public void Reset()
        {
            Timer.Reset();
            Active = false;
        }

        public void Update()
        {
            State = InputController.STATE_NONE;
            if (Active)
            {
                Timer.Update();
                Test();
            }
        }

        public void Start()
        {
            Active = true;
            if (UseStopwatch)
            {
                Timer.StopwatchTimer.Start();
            } else
            {
                Timer.FrameTimer.Start();
            }
        }

        private void Test()
        {
            if (InTime)
            {
                State = InputController.STATE_PRESSED;
            }
        }

        public bool InTime
        {
            get
            {
                if (UseStopwatch)
                {
                    if (Timer.CheckTime(_startTime, _endTime))
                    {
                        return true;
                    }
                }
                else
                {
                    if (Timer.CheckFrame(StartFrame, EndFrame))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            if (UseStopwatch)
            {
                Data.AddBoolean(SER_USESTOPWATCH, UseStopwatch);
            }
            if (StartFrame != -1)
            {
                Data.AddInt(SER_STARTFRAME, StartFrame);
            }
            if (EndFrame != -1)
            {
                Data.AddInt(SER_ENDFRAME, EndFrame);
            }
            if (StartMilliseconds != 0)
            {
                Data.AddLong(SER_STARTTIME, StartMilliseconds);
            }
            if (EndMilliseconds != 0)
            {
                Data.AddLong(SER_ENDTIME, EndMilliseconds);
            }

            return Data;
        }


        public void Deserialize(MaxSerializedObject Data)
        {
            if (Data.ContainsKey(SerializeID))
            {
                Data = Data.GetSerializedObject(SerializeID);
            }
            UseStopwatch = Data.GetBoolean(SER_USESTOPWATCH);
            StartFrame = Data.GetInt(SER_STARTFRAME);
            EndFrame = Data.GetInt(SER_ENDFRAME);
            StartMilliseconds = Data.GetLong(SER_STARTTIME);
            EndMilliseconds = Data.GetLong(SER_ENDTIME);
            Active = true;
        }

        public IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new BasicTimer();
            a.Deserialize(Data);
            return a;
        }
    }
}
