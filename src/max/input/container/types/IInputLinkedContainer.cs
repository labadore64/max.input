﻿using max.input.container.types.component;
using System.Collections.Generic;

namespace max.input.container.types
{
    public interface IInputLinkedContainer : IInputContainer
    {

        /// <summary>
        /// Activates the InputLinkedController to start listening for inputs, starting
        /// from the specified index.
        /// </summary>
        /// <param name="Index">Index to start from</param>
        void Activate(int Index);

        /// <summary>
        /// Goes to the next linked input in the sequence.
        /// </summary>
        void Next();

        /// <summary>
        /// Goes to the previous linked input in the sequence, if not null.
        /// </summary>
        void Previous();

        /// <summary>
        /// 
        /// </summary>
        List<LinkedInput> Inputs { get; }

        #region Manipulation methods
        int IndexOf(LinkedInput Input);
        #endregion
    }
}
