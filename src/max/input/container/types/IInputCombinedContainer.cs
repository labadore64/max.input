﻿using System.Collections.Generic;

namespace max.input.container.types
{
    public interface IInputCombinedContainer : IInputContainer
    {
        List<IInputContainer> Inputs { get; }
    }
}
