﻿using Microsoft.Xna.Framework.Input;

namespace max.input.container.types
{
    public interface IInputBasicContainer : IInputContainer
    {
        void SetInput(object Key);
    }
}
