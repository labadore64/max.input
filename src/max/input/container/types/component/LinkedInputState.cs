﻿namespace max.input.container.types.component
{
    public struct LinkedInputState
    {
        LinkedInput Parent;
        int State;

        public LinkedInputState(LinkedInput InputContainer, int State)
        {
            Parent = InputContainer;
            this.State = State;
        }
    }
}
