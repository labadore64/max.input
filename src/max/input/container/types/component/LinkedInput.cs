﻿
using max.input.container.keyboard;
using max.input.key;
using max.input.key.keyboard;
using max.serialize;
using max.time.timer;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using max.ds;
using max.util;
using static max.input.container.keyboard.LinkedKeyboardInputContainer;

namespace max.input.container.types.component
{
    /// <summary>
    /// Represents one linked input
    /// </summary>
    public class LinkedInput : IMaxSerializable
    {
        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public bool Serializable { get; set; } = true;

        const string SER_STARTFRAME = "startFrame";
        const string SER_ENDFRAME = "endFrame";
        const string SER_STARTTIME = "startTime";
        const string SER_ENDTIME = "endTime";
        const string SER_USESTOPWATCH = "useStopwatch";
        const string SER_PREVIOUS = "previous";
        const string SER_NEXT = "next";
        const string SER_CONTINUE = "continue";
        const string SER_IGNOREDKEYS = "ignoredInputs";
        const string SER_INPUT = "input";
        const string SER_TRIGGER = "trigger";
        const string SER_TEST = "test";

        public LinkedKeyboardInputContainer.TestDelegate TestTiming { get; set; } = null;

        public LinkedKeyboardInputContainer.TestDelegate TestTrigger { get; set; } = null;

        public const string SerializeID = "InputLink";

        /// <summary>
        /// Represents the base state of the input.
        /// </summary>
        public const int STATE_INACTIVE = 0;
        /// <summary>
        /// Represents the input waiting to listen for input.
        /// </summary>
        public const int STATE_WAITING = 1;
        /// <summary>
        /// Represents the input being triggered at the right time.
        /// </summary>
        public const int STATE_SUCCESS = 2;
        /// <summary>
        /// Represents the input being triggered at the wrong time.
        /// </summary>
        public const int STATE_FAILURE = 3;

        #region State Variables

        /// <summary>
        /// Represents the state of this input. 
        /// </summary>
        public int State { get; private set; } = STATE_INACTIVE;

        /// <summary>
        /// If true, signals that it should continue to the next input
        /// if it fails.
        /// </summary>
        public bool ContinueIfFail { get; set; }

        #endregion

        #region Timer variables

        StopwatchTimer Stopwatch = new StopwatchTimer();
        FrameTimer FrameTimer = new FrameTimer();

        long _stopwatchStartTicks;
        long _stopwatchEndTicks;
        public int FrameStart { get; set; }
        public int FrameEnd { get; set; }

        public long StopwatchStartTime
        {
            get
            {
                return (long)Math.Round(_stopwatchStartTicks * .00001);
            }
            set
            {
                _stopwatchStartTicks = value * 10000;
            }
        }

        public long StopwatchEndTime
        {
            get
            {
                return (long)Math.Round(_stopwatchEndTicks * .00001);
            }
            set
            {
                _stopwatchEndTicks = value * 10000;
            }
        }

        /// <summary>
        /// Whether or not to use the stopwatch for this input.
        /// </summary>
        public bool UseStopwatch { get; set; }

        #endregion

        #region Control Variables

        public IInputLinkedContainer Parent { get; private set; }

        /// <summary>
        /// The linked input to verify.
        /// </summary>
        public IInput Input { get; set; }

        /// <summary>
        /// The next LinkedInput to go to if this input is successful.
        /// </summary>
        public LinkedInput InputNext { get; set; }

        int _inputNext = -1;
        /// <summary>
        /// The previous LinkedInput that jumped to this LinkedInput.
        /// </summary>
        public LinkedInput InputPrevious { get; set; }
        int _inputPrevious = -1;
        /// <summary>
        /// List of inputs that should be ignored when evaluating whether
        /// an input succeeded or failed. If the input type is not the same
        /// as what is expected by the container, it will throw an error.
        /// </summary>
        public List<IInput> IgnoredInputs { get; private set; } = new List<IInput>();

        #endregion

        #region Constructor
        /// <summary>
        /// Empty Constructor.
        /// </summary>
        public LinkedInput()
        {

        }

        public LinkedInput(IInputLinkedContainer Parent)
        {
            this.Parent = Parent;
        }

        public LinkedInput(IInputLinkedContainer Parent, MaxSerializedObject Data)
        {
            this.Parent = Parent;
            Deserialize(Data);
        }
        #endregion

        #region Control Methods
        public void Reset()
        {
            State = STATE_INACTIVE;
            Stopwatch.Reset();
            FrameTimer.Reset();
        }

        public void Activate()
        {
            State = STATE_WAITING;
            if (UseStopwatch)
            {
                Stopwatch.Start();
            } else
            {
                FrameTimer.Start();
            }
        }

        public void Update()
        {
            Input.Update();
            if (!UseStopwatch)
            {
                FrameTimer.Update();
            }
        }
        #endregion

        #region Test Methods
        public bool TestInTimer()
        {
            if (UseStopwatch)
            {
                return Stopwatch.CheckTime(StopwatchStartTime, StopwatchEndTime);
            } else
            {
                return FrameTimer.CheckFrame(FrameStart,FrameEnd);
            }
        }
        #endregion

        #region Serialize Methods
        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            if (FrameStart != 0)
            {
                Data.AddInt(SER_STARTFRAME , FrameStart);
            }
            if (FrameEnd != 0)
            {
                Data.AddInt(SER_ENDFRAME , FrameEnd);
            }
            if (StopwatchStartTime != 0)
            {
                Data.AddLong(SER_STARTTIME, StopwatchStartTime);
            }
            if (StopwatchEndTime != 0)
            {
                Data.AddLong(SER_ENDTIME, StopwatchEndTime);
            }
            if (UseStopwatch)
            {
                Data.AddBoolean(SER_USESTOPWATCH, UseStopwatch);
            }
            if (InputPrevious != null)
            {
                Data.AddInt(SER_PREVIOUS, Parent.IndexOf(InputPrevious));
            }
            if (InputNext != null)
            {
                Data.AddInt(SER_NEXT, Parent.IndexOf(InputNext));
            }
            if (ContinueIfFail)
            {
                Data.AddBoolean(SER_CONTINUE, ContinueIfFail);
            }
            Data.AddSerializedObject(SER_INPUT, Input);

            List<IInput> ignored = IgnoredInputs;
            Keys[] ignoredKeys = new Keys[ignored.Count];
            for(int i = 0; i < ignored.Count; i++)
            {
                ignoredKeys[i] = (Keys)ignored[i].Source;
            }
            if (ignoredKeys.Length > 0)
            {
                Data.AddKeyCollection(SER_IGNOREDKEYS, ignoredKeys);
            }

            if(TestTrigger != null)
            {
                Data.AddFullMethod(SER_TRIGGER, TestTrigger.Method);
            }
             if (TestTiming != null)
            {
                Data.AddFullMethod(SER_TEST, TestTiming.Method);
            }

            return Data;
        }

        internal void SetPreviousNext()
        {
            if(_inputNext == -1)
            {
                InputNext = null;
            } else
            {
                InputNext = Parent.Inputs[_inputNext];
            }

            if (_inputPrevious == -1)
            {
                InputPrevious = null;
            }
            else
            {
                InputPrevious = Parent.Inputs[_inputPrevious];
            }
        }

        public void Deserialize(MaxSerializedObject Data)
        {
            FrameStart = 0;
            FrameEnd = 0;
            StopwatchStartTime = 0;
            StopwatchEndTime = 0;
            UseStopwatch = false;
            IgnoredInputs.Clear();
            InputNext = null;
            InputPrevious = null;

            FrameStart = Data.GetInt(SER_STARTFRAME);

            FrameEnd = Data.GetInt(SER_ENDFRAME);

            StopwatchStartTime = Data.GetLong(SER_STARTTIME);
            StopwatchEndTime = Data.GetLong(SER_ENDTIME);
            UseStopwatch = Data.GetBoolean(SER_USESTOPWATCH);
            _inputNext = Data.GetInt(SER_NEXT);
            _inputPrevious= Data.GetInt(SER_PREVIOUS);
            ContinueIfFail = Data.GetBoolean(SER_CONTINUE);

            if (Data.ContainsKey(SER_IGNOREDKEYS))
            {
                Keys[] ignoredKeys = Data.GetKeysList(SER_IGNOREDKEYS).ToArray();
                for(int i = 0; i < ignoredKeys.Length; i++)
                {
                    IgnoredInputs.Add(new KeyInput(ignoredKeys[i], Parent));
                }
            }
            if (Data.ContainsKey(SER_INPUT))
            {
                Input = (IInput)Data.GetSerializedObject(SER_INPUT).Deserialize();
            }

            if (Data.ContainsKey(SER_TRIGGER))
            {
                TestTrigger = (TestDelegate)Data.GetFullMethod(SER_TRIGGER).CreateDelegate(typeof(TestDelegate));
            }

            if (Data.ContainsKey(SER_TEST))
            {
                TestTiming = (TestDelegate)Data.GetFullMethod(SER_TEST).CreateDelegate(typeof(TestDelegate));
            }
        }

        public IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new LinkedInput();
            a.Deserialize(Data);
            return a;
        }

        #endregion
    }
}
