﻿using max.input.handler;
using max.serialize;

namespace max.input.container
{
    public interface IInputContainer : IMaxSerializable
    {
        /// <summary>
        /// The input group that contains this container's inputs.
        /// </summary>
        IInputHandler ParentInputHandler { get; set; }

        /// <summary>
        /// The parent input container to this one. Used mostly with Combined Inputs.
        /// </summary>
        IInputContainer ParentContainer { get; set; }

        /// <summary>
        /// Whether or not this input container is active.
        /// </summary>
        bool Active { get; set; }

        /// <summary>
        /// The returned value of the input state.
        /// </summary>
        float[] Value { get; }
        /// <summary>
        /// The source value of the input this frame.
        /// </summary>
        float[] SourceValue { get; }

        /// <summary>
        /// The shorthand input state for the whole container.
        /// </summary>
        float State { get; }

        /// <summary>
        /// The state of the input after taking into consideration timers.
        /// </summary>
        float TimerState { get; }


        /// <summary>
        /// Activates the input container to start listening for inputs.
        /// </summary>
        void Activate();

        /// <summary>
        /// Updates the input state this frame.
        /// </summary>
        void Update();

        void Reset();

    }
}
