﻿using max.input.container.types;
using max.input.handler.keyboard;
using max.input.key;
using max.input.key.keyboard;
using max.input.key.timer;
using max.serialize;
using max.util;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace max.input.container.keyboard
{
    /// <summary>
    /// The BasicKeyboardInputContainer acts as a pass-through between the InputGroup and
    /// the KeyInput. As such most of its values reflect the state of the key bound to
    /// the input.
    /// </summary>
    public class BasicKeyboardInputContainer : KeyboardContainer, IInputBasicContainer
    {
        public override float[] Value { get { return Key.Value; } }

        public override float[] SourceValue { get { return Key.SourceValue; } }

        public override float State { get { return Key.State; } }

        public override float TimerState { get { if (Key.Timer.Active) { return Key.Timer.State; } else { return State; } } }

        public InputTimer Timer { get { return Key.Timer; } }


        KeyInput Key;

        public delegate bool UpdateDelegate(BasicKeyboardInputContainer Container);

        public UpdateDelegate UpdateMethod { get; set; }

        const string SER_INPUT = "input";
        const string SER_UPDATE_METHOD = "updateMethod";

        public List<IInput> Inputs
        {
            get
            {
                List<IInput> returner = new List<IInput>();
                returner.Add(Key);
                return returner;
            }

        }

        #region Constructors

        public BasicKeyboardInputContainer() : base() { }

        public BasicKeyboardInputContainer(KeyboardHandler Parent)
        {
            SetInput('A');
            ParentInputHandler = Parent;
        }

        public BasicKeyboardInputContainer(IInputContainer Parent)
        {
            ParentContainer = Parent;
        }

        public BasicKeyboardInputContainer(IInputContainer Parent, MaxSerializedObject Data)
        {
            ParentContainer = Parent;
            Deserialize(Data);
        }

        public BasicKeyboardInputContainer(KeyboardHandler Parent, MaxSerializedObject Data)
        {
            Deserialize(Data);
            ParentInputHandler = Parent;
        }
        /// <summary>
        /// Create a new KeyData with a character value.
        /// </summary>
        /// <param name="Character">Character</param>
        public BasicKeyboardInputContainer(char Character, KeyboardHandler Parent)
        {
            SetInput(Character);
            ParentInputHandler = Parent;
        }

        /// <summary>
        /// Create a new KeyData with a character value.
        /// </summary>
        /// <param name="Character">Character</param>
        public BasicKeyboardInputContainer(char Character, IInputContainer Parent)
        {
            SetInput(Character);
            ParentContainer = Parent;
        }


        /// <summary>
        /// Create a new KeyData with a character value.
        /// </summary>
        /// <param name="Character">Character</param>
        public BasicKeyboardInputContainer(string Character, KeyboardHandler Parent)
        {
            SetInput(Character);
            ParentInputHandler = Parent;
        }

        /// <summary>
        /// Create a new KeyData with a character value.
        /// </summary>
        /// <param name="Character">Character</param>
        public BasicKeyboardInputContainer(string Character, IInputContainer Parent)
        {
            SetInput(Character);
            ParentContainer = Parent;
        }

        /// <summary>
        /// Create a new KeyData with XNA Keys values.
        /// </summary>
        /// <param name="Key">Key</param>
        public BasicKeyboardInputContainer(Keys Key, KeyboardHandler Parent)
        {
            SetInput(Key);
            ParentInputHandler = Parent;
        }

        /// <summary>
        /// Create a new KeyData with XNA Keys values.
        /// </summary>
        /// <param name="Key">Key</param>
        public BasicKeyboardInputContainer(Keys Key, IInputContainer Parent)
        {
            SetInput(Key);
            ParentContainer = Parent;
        }

        #endregion

        /// <summary>
        /// Updates the state of the container.
        /// </summary>
        public override void Update()
        {
            if (Active)
            {
                if (UpdateMethod != null)
                {
                    UpdateMethod.Invoke(this);
                }
                else
                {
                    Key.Update();
                }
            }
        }

        public void SetInput(string Character)
        {
            Key = new KeyInput((Keys)Character.ToUpper().ToCharArray()[0], this);
        }

        public void SetInput(char Character)
        {
            Key = new KeyInput((Keys)char.ToUpper(Character), this);
        }

        public void SetInput(Keys Character)
        {
            Key = new KeyInput(Character, this);
        }

        public void SetInput(object Key)
        {
            if(Key.GetType() != typeof(Keys))
            {
                throw new ArgumentException("Source must be of type " + typeof(Keys).FullName);
            }
            this.Key = new KeyInput((Keys)Key, this);
        }

        public override void Reset()
        {
            Key.Reset();
        }

        public override void Activate()
        {
            Active = true;
        }

        public override MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            Data.SetType(this);
            Data.AddSerializedObject(SER_INPUT, Key);
            if (UpdateMethod != null)
            {
                Data.AddString(SER_UPDATE_METHOD, ReflectionUtil.DelegateToString(UpdateMethod));
            }
            return Data;
        }

        public override void Deserialize(MaxSerializedObject Data)
        {

            Key = (KeyInput)Data.GetSerializedObject(SER_INPUT).Deserialize();
            Key.Parent = this;
            if (Data.ContainsKey(SER_UPDATE_METHOD))
            {
                MethodInfo info = ReflectionUtil.FullStringToMethod(Data.GetString(SER_UPDATE_METHOD));
                UpdateMethod = (UpdateDelegate)info.CreateDelegate(typeof(UpdateDelegate));
            }
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new BasicKeyboardInputContainer();
            a.Deserialize(Data);
            return a;
        }
    }
}
