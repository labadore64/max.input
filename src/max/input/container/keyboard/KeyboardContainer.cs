﻿using max.input.handler;
using max.serialize;

namespace max.input.container.keyboard
{
    public abstract class KeyboardContainer : IInputContainer
    {
        #region Properties

        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Serializable { get; set; } = true;

        /// <summary>
        /// The parent input handler, if any.
        /// </summary>
        /// <value>Parent</value>
        public IInputHandler ParentInputHandler { get; set; }
        /// <summary>
        /// The parent container, if any.
        /// </summary>
        /// <value>Parent</value>
        public IInputContainer ParentContainer { get; set; }

        /// <summary>
        /// Whether or not the current container is active.
        /// </summary>
        /// <value>True/False</value>
        public bool Active { get; set; } = true;

        /// <summary>
        /// The current values of the input, modified for timing.
        /// </summary>
        /// <value>Value array</value>
        public virtual float[] Value { get; protected set; }

        /// <summary>
        /// The base values of the input.
        /// </summary>
        /// <value>Value array</value>
        public virtual float[] SourceValue { get; protected set; }

        /// <summary>
        /// For this container, the state only returns InputController.STATE_PRESSED
        /// if the input was completed this frame. Otherwise, it will return
        /// InputController.STATE_NONE.
        /// </summary>
        public virtual float State { get; protected set; }
        public abstract float TimerState { get; }

        #endregion

        #region Constructors

        public KeyboardContainer()
        {

        }

        #endregion

        #region Controller
        public abstract void Activate();
        public abstract void Update();
        public abstract void Reset();

        #endregion

        #region Serialize

        public abstract MaxSerializedObject Serialize();
        public abstract void Deserialize(MaxSerializedObject Data);
        public abstract IMaxSerializable CreateInstance(MaxSerializedObject Data);

        #endregion
    }
}
