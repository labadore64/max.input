﻿using System.Collections.Generic;
using max.input.container.types;
using max.input.handler.keyboard;
using max.input.key.timer;
using max.serialize;

namespace max.input.container.keyboard
{
    public class CombinedKeyboardInputContainer : KeyboardContainer, IInputCombinedContainer
    {

        const string SER_LIST = "inputs";
        const string SER_TIMER = "timer";

        /// <summary>
        /// This timer allows you to have the linked timer have a time limit
        /// to complete all inputs, otherwise it resets.
        /// </summary>
        public BasicTimer Timer { get; private set; } = new BasicTimer();

        public override float TimerState { get { return Timer.State; } }

        public List<IInputContainer> Inputs { get; protected set; } = new List<IInputContainer>();

        #region Constructors

        public CombinedKeyboardInputContainer() : base() { }

        public CombinedKeyboardInputContainer(KeyboardHandler Parent)
        {
            ParentInputHandler = Parent;
        }

        public CombinedKeyboardInputContainer(IInputContainer Parent)
        {
            ParentContainer = Parent;
        }

        public CombinedKeyboardInputContainer(IInputContainer Parent, MaxSerializedObject Data)
        {
            ParentContainer = Parent;
            Deserialize(Data);
        }

        public CombinedKeyboardInputContainer(KeyboardHandler Parent, MaxSerializedObject Data)
        {
            ParentInputHandler = Parent;
            Deserialize(Data);
        }

        #endregion

        public void Add(IInputContainer Container)
        {
            Inputs.Add(Container);
        }

        public void Remove(IInputContainer Container)
        {
            Inputs.Remove(Container);
        }

        public void RemoveAt(int Index)
        {
            Inputs.RemoveAt(Index);
        }

        public override void Reset()
        {
            for (int i = 0; i < Inputs.Count; i++)
            {
                Inputs[i].Reset();
            }

            Timer.Reset();
        }

        public override void Update()
        {
            if (Active)
            {
                bool pressed = true;
                bool released = false;
                for (int i = 0; i < Inputs.Count; i++)
                {
                    Inputs[i].Update();
                    if(Inputs[i].TimerState != InputController.STATE_PRESSED)
                    {
                        pressed = false;
                    }
                    if(Inputs[i].TimerState == InputController.STATE_RELEASED)
                    {
                        released = true;
                    }
                }

                Timer.Update();

                if (pressed)
                {
                    Value = new float[] { InputController.STATE_PRESSED};
                }
                if (released)
                {
                    Value = new float[] { InputController.STATE_RELEASED };
                } else
                {
                    Value = new float[] { InputController.STATE_NONE };
                }


                bool missTiming = false;

                if (Timer.IsSet)
                {
                    if (!Timer.InTime)
                    {
                        missTiming = true;
                    }
                }

                if ((missTiming && Value[0] == InputController.STATE_PRESSED) || Value[0] == InputController.STATE_RELEASED)
                {
                    SimulateRelease();
                }

            }
        }

        protected void SimulateRelease()
        {
            Value[0] = InputController.STATE_RELEASED;
            Reset();
        }

        public override void Activate()
        {
            Active = true;
        }

        public override MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            Data.SetType(this);
            if (Timer.IsSet) {
                Data.AddSerializedObject(SER_TIMER, Timer);
            }

            MaxSerializedObject[] list = new MaxSerializedObject[Inputs.Count];
            for(int i = 0; i < list.Length; i++)
            {
                list[i] = Inputs[i].Serialize();
            }

            if(Inputs.Count > 0)
            {
                Data.AddSerializedObjectCollection(SER_LIST, list);
            }
            return Data;
        }

        public override void Deserialize(MaxSerializedObject Data)
        {
           
            if (Data.ContainsKey(SER_TIMER))
            {
                Timer = (BasicTimer)Data.GetSerializedObject(SER_TIMER).Deserialize();
            } else
            {
                Timer = new BasicTimer();
            }

            if (Data.ContainsKey(SER_LIST))
            {
                List<MaxSerializedObject> objs = Data.GetSerializedObjectCollection(SER_LIST);

                for(int i = 0; i < objs.Count; i++)
                {
                    Inputs.Add((IInputContainer)objs[i].Deserialize());
                }
            }
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new CombinedKeyboardInputContainer();
            a.Deserialize(Data);
            return a;
        }

    }
}
