﻿using System;
using System.Collections.Generic;
using max.input.container.types;
using max.input.container.types.component;
using max.input.handler.keyboard;
using Microsoft.Xna.Framework.Input;
using max.input.key;
using max.input.key.keyboard;
using max.input.key.timer;
using System.Reflection;
using max.util;
using max.serialize;

namespace max.input.container.keyboard
{
    public class LinkedKeyboardInputContainer : KeyboardContainer, IInputLinkedContainer
    {

        const string SER_TESTTRIGGER = "triggerMethod";
        const string SER_TESTTIMING = "timingMethod";
        const string SER_TIMER = "timer";

        /// <summary>
        /// This timer allows you to have the linked timer have a time limit
        /// to complete all inputs, otherwise it resets.
        /// </summary>
        public BasicTimer Timer { get; private set; } = new BasicTimer();

        public delegate bool TestDelegate(LinkedKeyboardInputContainer Container);

        /// <summary>
        /// Holds the method used to test whether or not the current input 
        /// succeeded.
        /// </summary>
        /// <returns>Whether the input passed timing or not.</returns>
        public TestDelegate TestTiming;
        /// <summary>
        /// Holds the method used to test whether or not the current input should be tested
        /// </summary>
        /// <returns>Whether the input is tested this cycle or not.</returns>
        public TestDelegate TestTrigger;

        public List<LinkedInput> Inputs { get; set; } = new List<LinkedInput>();

        //this input is just a holding variable for the current input being modified.
        //if you add a linked input to the input list, this will set that last linked input
        //to this variable. Used for things like the linked input managing functions.
        public LinkedInput CurrentModifiedInput { get; private set; } 

        public List<LinkedInputState> CompletedInputStates { get; protected set; } = new List<LinkedInputState>();

        public override float TimerState { get { return State; } }

        LinkedInput CurrentInput { get; set; }

        bool _completed = false;
        bool _failed = false;

        #region Constructors

        public LinkedKeyboardInputContainer() : base() { }

        public LinkedKeyboardInputContainer(KeyboardHandler Parent)
        {
            ParentInputHandler = Parent;
            TestTrigger = DefaultTrigger;
            TestTiming = DefaultTest;
        }

        public LinkedKeyboardInputContainer(KeyboardHandler Parent, MaxSerializedObject Data)
        {
            ParentInputHandler = Parent;
            TestTrigger = DefaultTrigger;
            TestTiming = DefaultTest;
            Deserialize(Data);
        }

        public LinkedKeyboardInputContainer(IInputContainer Parent)
        {
            TestTrigger = DefaultTrigger;
            TestTiming = DefaultTest;
            ParentContainer = Parent;
        }

        public LinkedKeyboardInputContainer(IInputContainer Parent, MaxSerializedObject Data)
        {
            ParentContainer = Parent;
            TestTrigger = DefaultTrigger;
            TestTiming = DefaultTest;
            Deserialize(Data);
        }

        #endregion

        public override void Reset()
        {
            CompletedInputStates.Clear();
            Active = false;
            CurrentInput = null;
        }

        public override void Update()
        {
            if (Active && CurrentInput != null)
            {

                CurrentInput.Update();
                Timer.Update();
                TestDelegate testTrigger = CurrentInput.TestTrigger;
                if(testTrigger == null)
                {
                    testTrigger = TestTrigger;
                }

                TestDelegate testTiming = CurrentInput.TestTiming;
                if (testTiming == null)
                {
                    testTiming = TestTiming;
                }

                //tests if the trigger was set.
                //this is usually the default trigger method but it
                //can be custom set to a unique trigger.
                if (testTrigger.Invoke(this))
                {
                    //tests if the trigger occurs within the timer.
                    //the timer will only be tested if the associated
                    //timer (frame/stopwatch) is set and has values for the
                    //start or end times.
                    if (TimerTest())
                    {
                        //tests whether or not the link passes.
                        //this is usually the default test but it can be 
                        //set to a unique trigger.
                        if (testTiming.Invoke(this))
                        {
                            PassTest();
                        }
                        else
                        {
                            FailTest();
                        }
                    }
                }
            }

            if (_completed)
            {
                _completed = false;
                State = InputController.STATE_PRESSED;
            }
            else if (_failed)
            {
                _failed = false;
                State = InputController.STATE_RELEASED;
            }
            else
            {
                State = InputController.STATE_NONE;
            }

        }

        //tests if the timer should exit.
        private bool TimerTest()
        {
            if (Timer.IsSet)
            {
                if(Timer.State == InputController.STATE_NONE)
                {
                    Reset();
                    _failed = true;
                    return false;
                }
            }

            return true;
        }

        public override void Activate()
        {
            //reset the state of the linked input when activating.
            Reset();
            CurrentInput = Inputs[0];
            CurrentInput.Reset();
            CurrentInput.Activate();
            Active = true;
            Timer.Start();
        }

        public void Activate(int Index)
        {
            //reset the state of the linked input when activating.
            Reset();
            CurrentInput = Inputs[Index];
            CurrentInput.Reset();
            CurrentInput.Activate();
            Active = true;
            Timer.Start();
        }

        public void Activate(LinkedInput Input)
        {
            if (Inputs.Contains(Input))
            {
                Reset();
                CurrentInput = Input;
                CurrentInput.Reset();
                CurrentInput.Activate();
                Active = true;
                Timer.Start();
            } else
            {
                throw new ArgumentException("Linked Input must be contained in this container.");
            }
        }

        public void Next()
        {
            CurrentInput = CurrentInput.InputNext;
            ValidateLinkedInput(CurrentInput);
            if(CurrentInput == null)
            {
                CompleteInput();
            } else
            {
                CurrentInput.Reset();
                CurrentInput.Activate();
            }
        }

        public void Previous()
        {
            if(CurrentInput.InputPrevious != null)
            {
                CurrentInput = CurrentInput.InputPrevious;
                ValidateLinkedInput(CurrentInput);
                CurrentInput.Reset();
                CurrentInput.Activate();
            }
        }

        /// this represents the completed input state.
        private void CompleteInput()
        {
            _completed = true;
            Active = false;
        }

        private void AddResult(LinkedInputState State)
        {
            CompletedInputStates.Add(State);
        }

        private void PassTest()
        {
            //add state of this input
            AddResult(new LinkedInputState(CurrentInput, LinkedInput.STATE_SUCCESS));
            Next();
        }

        private void FailTest()
        {
            //add state of this input
            AddResult(new LinkedInputState(CurrentInput, LinkedInput.STATE_FAILURE));

            //If this input allows for the next one to continue,
            //just go to the next input, although this one will be marked
            //as failed. The state does not return "released" if this occurs.
            if (CurrentInput.ContinueIfFail)
            {
                Next();
            } else
            {
                _failed = true;
                Reset();
            }
        }
        
        //the default method for determining if the trigger passed or failed the linked test.
        //If its within the time frame defined in the Linked Input, it returns true. Otherwise
        //it returns false.
        public static bool DefaultTest(LinkedKeyboardInputContainer Container)
        {
            KeyboardHandler handler = (KeyboardHandler)Container.ParentInputHandler;
            List<Keys> combined = Container.GetActiveKeys(handler.CurrentState, handler.PreviousState);

            //test if the timer is within time.
            

            if(combined.Count > 1)
            {
                return false;
            }

            if (!combined[0].Equals((Keys)Container.CurrentInput.Input.Source))
            {
                return false;
            }

            return Container.CurrentInput.TestInTimer();
        }

        //the default method for whether or not this frame was Trigger.
        //If the input is pressed, it returns true, otherwise it returns false.
        public static bool DefaultTrigger(LinkedKeyboardInputContainer Container)
        {
            return Container.DefaultTriggerKeyboard((KeyboardHandler)Container.ParentInputHandler);
        }

        public bool DefaultTriggerKeyboard(KeyboardHandler handler)
        {
            List<Keys> combined = GetActiveKeys(handler.CurrentState, handler.PreviousState);

            if(combined == null)
            {
                return false;
            }

            //if there are keys something was pressed.
            if (combined.Count > 0)
            {
                return true;
            }

            return false;
        }

        private List<Keys> GetActiveKeys(KeyboardState states, KeyboardState prevstates)
        {
            //get the current input state. it checks just if the key was pressed this frame.
            List<Keys> inputState = new List<Keys>(states.GetPressedKeys());

            //if there is nothing in the input then return false
            if (inputState.Count == 0)
            {
                return null;
            }

            List<Keys> inputPrevState = new List<Keys>(prevstates.GetPressedKeys());

            //make a list of keys if both are present in both lists
            List<Keys> combined = new List<Keys>();

            for (int i = 0; i < inputState.Count; i++)
            {
                //if the previous input doesn't contain the 
                //current input, the state is pressed.
                //you can add it to the combined list.
                if (!inputPrevState.Contains(inputState[i]))
                {
                    combined.Add(inputState[i]);
                }
            }

            //if the combined list is empty return false
            if (combined.Count == 0)
            {
                return null;
            }


            List<IInput> ignoredInputs = CurrentInput.IgnoredInputs;
            Keys[] ignoredKeys = new Keys[ignoredInputs.Count];

            //get the ignored keys as keys
            for (int i = 0; i < ignoredInputs.Count; i++)
            {
                ignoredKeys[i] = (Keys)ignoredInputs[i].Source;
            }

            //remove all the keys that are in the ignored keys array

            for (int i = 0; i < ignoredKeys.Length; i++)
            {
                if (combined.Contains(ignoredKeys[i]))
                {
                    combined.Remove(ignoredKeys[i]);
                }
            }

            return combined;
        }

        /// <summary>
        /// Sets the trigger method for this input. This method must
        /// have 0 arguments and return a bool value. If the method returns true, the input
        /// has been Trigger, and it will test to see if it passes or
        /// fails the conditions set by the test method.
        /// </summary>
        /// <param name="method">The method to test if it was Trigger</param>
        public void SetTriggerMethod(TestDelegate method)
        {
            TestTrigger = method;
        }

        /// <summary>
        /// Sets the test method for this input. This method must
        /// have 0 arguments and return a bool value. If the method returns true,
        /// the input will be treated as successful. Otherwise, the input would fail.
        /// The input is completed and moves to the next input if successful, or if
        /// the failed input always moves to the next input. If there are no more inputs,
        /// the linked input completes and that frame only it is marked as "pressed".
        /// Otherwise the linked input resets.
        /// </summary>
        /// <param name="method">The method to test if it was Trigger</param>
        public void SetTestMethod(TestDelegate method)
        {
            TestTrigger = method;
        }

        #region builder methods for the linked inputs.

        //todo: add methods to easily add, remove, and modify linked inputs.
        /// <summary>
        /// Adds an input into the input list.
        /// </summary>
        /// <param name="Input">Input</param>
        public LinkedInput InputAdd(IInput Input)
        {
            LinkedInput input = new LinkedInput(this);
            input.Input = Input;
            return InputAdd(input);
        }

        public LinkedInput InputAdd(char Character)
        {
            KeyInput input = new KeyInput(Character, this);
            return InputAdd(input);
        }

        public LinkedInput InputAdd(String Character)
        {
            KeyInput input = new KeyInput(Character.ToCharArray()[0], this);
            return InputAdd(input);
        }

        public LinkedInput InputAdd(Keys Key)
        {
            KeyInput input = new KeyInput(Key,this);
            return InputAdd(input);
        }

        /// <summary>
        /// Adds a linked input into the input list.
        /// </summary>
        /// <param name="Input">Input</param>
        public LinkedInput InputAdd(LinkedInput Input)
        {
            if (Inputs.Count > 0)
            {
                Input.InputPrevious = Inputs[Inputs.Count - 1];
                if (Inputs[Inputs.Count - 1].InputNext == null)
                {
                    Inputs[Inputs.Count - 1].InputNext = Input;
                }
            }
            Inputs.Add(Input);
            CurrentModifiedInput = Input;

            return Input;
        }

        //validates if this linked input is in the linked input list
        private void ValidateLinkedInput(LinkedInput input)
        {
            if(input == null)
            {
                return;
            }
            if (!Inputs.Contains(input))
            {
                throw new ArgumentException("This linked input is not in this container's input list, and is therefore invalid.");
            }
        }

        public void InputStartTime(long Milliseconds)
        {
            InputStartTime(CurrentModifiedInput, Milliseconds);
        }

        public void InputStartTime(int Index, long Milliseconds)
        {
            InputStartTime(Inputs[Index], Milliseconds);
        }

        public void InputStartTime(LinkedInput input, long Milliseconds)
        {
            ValidateLinkedInput(input);
            if (input != null)
            {
                input.StopwatchStartTime = Milliseconds;
            }
        }

        public void InputEndTime(long Milliseconds)
        {
            InputEndTime(CurrentModifiedInput, Milliseconds);
        }

        public void InputEndTime(int Index, long Milliseconds)
        {
            InputEndTime(Inputs[Index], Milliseconds);
        }

        public void InputEndTime(LinkedInput input, long Milliseconds)
        {
            ValidateLinkedInput(input);
            if (input != null)
            {
                input.StopwatchStartTime = Milliseconds;
            }
        }

        public void InputStartFrame(int Frame)
        {
            InputStartFrame(CurrentModifiedInput, Frame);
        }

        public void InputStartFrame(int Index, int Frame)
        {
            InputStartFrame(Inputs[Index], Frame);
        }

        public void InputStartFrame(LinkedInput input, int Frame)
        {
            ValidateLinkedInput(input);
            if (input != null)
            {
                input.FrameStart = Frame;
            }
        }

        public void InputEndFrame(int Frame)
        {
            InputEndFrame(CurrentModifiedInput, Frame);
        }

        public void InputEndFrame(int Index, int Frame)
        {
            InputEndFrame(Inputs[Index], Frame);
        }

        public void InputEndFrame(LinkedInput input, int Frame)
        {
            ValidateLinkedInput(input);
            if (input != null)
            {
                input.FrameEnd = Frame;
            }
        }

        public void InputEnableStopwatch()
        {
            InputEnableStopwatch(CurrentModifiedInput);
        }

        public void InputEnableStopwatch(int Index)
        {
            InputEnableStopwatch(Inputs[Index]);
        }

        public void InputEnableStopwatch(LinkedInput input)
        {
            ValidateLinkedInput(input);
            if(input != null)
            {
                input.UseStopwatch = true;
            }
        }

        public void InputDisableStopwatch()
        {
            InputDisableStopwatch(CurrentModifiedInput);
        }

        public void InputDisableStopwatch(int Index)
        {
            InputDisableStopwatch(Inputs[Index]);
        }

        public void InputDisableStopwatch(LinkedInput input)
        {
            ValidateLinkedInput(input);
            if (input != null)
            {
                input.UseStopwatch = false;
            }
        }

        public void InputEnableContinueAfterFailure()
        {
            InputEnableContinueAfterFailure(CurrentModifiedInput);
        }

        public void InputEnableContinueAfterFailure(int Index)
        {
            InputEnableContinueAfterFailure(Inputs[Index]);
        }

        public void InputEnableContinueAfterFailure(LinkedInput input)
        {
            ValidateLinkedInput(input);
            if (input != null)
            {
                input.ContinueIfFail = true;
            }
        }

        public void InputDisableContinueAfterFailure()
        {
            InputEnableContinueAfterFailure(CurrentModifiedInput);
        }

        public void InputDisableContinueAfterFailure(int Index)
        {
            InputEnableContinueAfterFailure(Inputs[Index]);
        }


        public void InputDisableContinueAfterFailure(LinkedInput input)
        {
            ValidateLinkedInput(input);
            if (input != null)
            {
                input.ContinueIfFail = true;
            }
        }

        public void InputSetNext(int NextInputIndex)
        {
            InputSetNext(CurrentModifiedInput, Inputs[NextInputIndex]);
        }

        public void InputSetNext(LinkedInput NextInput)
        {
            InputSetNext(CurrentModifiedInput, NextInput);
        }

        public void InputSetNext(int InputIndex, int NextInputIndex)
        {
            InputSetNext(Inputs[InputIndex], Inputs[NextInputIndex]);
        }

        public void InputSetNext(LinkedInput Input, int NextInputIndex)
        {
            InputSetNext(Input, Inputs[NextInputIndex]);
        }

        public void InputSetNext(int InputIndex, LinkedInput NextInput)
        {
            InputSetNext(Inputs[InputIndex], NextInput);
        }


        public void InputSetNext(LinkedInput Input, LinkedInput NextInput)
        {
            ValidateLinkedInput(Input);
            ValidateLinkedInput(NextInput);
            if (Input != null)
            {
                Input.InputNext = NextInput;
            }
        }

        public void InputSetPrevious(int PreviousInputIndex)
        {
            InputSetPrevious(CurrentModifiedInput, Inputs[PreviousInputIndex]);
        }

        public void InputSetPrevious(LinkedInput PreviousInput)
        {
            InputSetPrevious(CurrentModifiedInput, PreviousInput);
        }

        public void InputSetPrevious(int InputIndex, int PreviousInputIndex)
        {
            InputSetPrevious(Inputs[InputIndex], Inputs[PreviousInputIndex]);
        }

        public void InputSetPrevious(LinkedInput Input, int PreviousInputIndex)
        {
            InputSetPrevious(Input, Inputs[PreviousInputIndex]);
        }

        public void InputSetPrevious(int InputIndex, LinkedInput PreviousInput)
        {
            InputSetPrevious(Inputs[InputIndex], PreviousInput);
        }

        public void InputSetPrevious(LinkedInput Input, LinkedInput PreviousInput)
        {
            ValidateLinkedInput(Input);
            ValidateLinkedInput(PreviousInput);
            if (Input != null)
            {
                Input.InputPrevious = PreviousInput;
            }
        }

        public void InputAddIgnored(IInput IgnoredInput)
        {
            InputAddIgnored(CurrentModifiedInput, IgnoredInput);
        }

        public void InputAddIgnored(int InputIndex, IInput IgnoredInput)
        {
            InputAddIgnored(Inputs[InputIndex], IgnoredInput);
        }

        public void InputAddIgnored(LinkedInput Input, IInput IgnoredInput)
        {
            ValidateLinkedInput(Input);
            if(Input != null)
            {
                Input.IgnoredInputs.Add(IgnoredInput);
            }
        }

        public void InputRemoveIgnored(IInput IgnoredInput)
        {
            InputRemoveIgnored(CurrentModifiedInput, IgnoredInput);
        }

        public void InputRemoveIgnored(int InputIndex, IInput IgnoredInput)
        {
            InputRemoveIgnored(Inputs[InputIndex], IgnoredInput);
        }

        public void InputRemoveIgnored(LinkedInput Input, IInput IgnoredInput)
        {
            ValidateLinkedInput(Input);
            if (Input != null)
            {
                Input.IgnoredInputs.Remove(IgnoredInput);
            }
        }

        public int IndexOf(LinkedInput Input)
        {
            ValidateLinkedInput(Input);
            if(Input != null)
            {
                return Inputs.IndexOf(Input);
            }

            return -1;
        }

        #endregion

        public override MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();

            Data.SetType(this);
            Data.AddSerializableObjectCollection(LinkedInput.SerializeID, Inputs.ToArray());
            if (TestTrigger != null)// && TestTrigger != DefaultTrigger)
            {
                Data.AddString(SER_TESTTRIGGER, ReflectionUtil.DelegateToString(TestTrigger));
            }
            if (TestTiming != null)// && TestTiming != DefaultTest)
            {
                Data.AddString(SER_TESTTIMING, ReflectionUtil.DelegateToString(TestTiming));
            }
            if (Timer.IsSet)
            {
                Data.AddSerializedObject(SER_TIMER, Timer);
            }
            return Data;
        }

        public override void Deserialize(MaxSerializedObject Data)
        {
            Inputs.Clear();
            Reset();
            if (Data.ContainsKey(LinkedInput.SerializeID))
            {
                object[] Links = (object[])Data[LinkedInput.SerializeID];
                for (int i = 0; i < Links.Length; i++)
                {
                    Inputs.Add(new LinkedInput(this, new MaxSerializedObject(Links[i])));
                }
                for(int i = 0; i < Inputs.Count; i++)
                {
                    Inputs[i].SetPreviousNext();
                }
            }
            if (Data.ContainsKey(SER_TIMER))
            {
                Timer = new BasicTimer(Data[SER_TIMER]);
            }
            if (Data.ContainsKey(SER_TESTTRIGGER))
            {
                MethodInfo info = ReflectionUtil.FullStringToMethod((string)Data[SER_TESTTRIGGER]);
                TestTrigger = (TestDelegate)info.CreateDelegate(typeof(TestDelegate));
            }
            if (Data.ContainsKey(SER_TESTTIMING))
            {
                MethodInfo info = ReflectionUtil.FullStringToMethod((string)Data[SER_TESTTIMING]);
                TestTiming = (TestDelegate)info.CreateDelegate(typeof(TestDelegate));
            }
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new LinkedKeyboardInputContainer();
            a.Deserialize(Data);
            return a;
        }
    }
}
