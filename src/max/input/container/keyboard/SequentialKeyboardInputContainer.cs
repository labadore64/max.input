﻿using max.input.handler.keyboard;
using max.serialize;

namespace max.input.container.keyboard
{
    public class SequentialKeyboardInputContainer : CombinedKeyboardInputContainer
    {

        int currentIndex = -1;

        #region Constructors

        public SequentialKeyboardInputContainer() : base() { }

        public SequentialKeyboardInputContainer(KeyboardHandler Parent)
        {
            ParentInputHandler = Parent;
        }

        public SequentialKeyboardInputContainer(IInputContainer Parent)
        {
            ParentContainer = Parent;
        }

        public SequentialKeyboardInputContainer(IInputContainer Parent, MaxSerializedObject Data)
        {
            ParentContainer = Parent;
            Deserialize(Data);
        }

        public SequentialKeyboardInputContainer(KeyboardHandler Parent, MaxSerializedObject Data)
        {
            ParentInputHandler = Parent;
            Deserialize(Data);
        }

        #endregion

        public override void Activate()
        {
            base.Activate();
            currentIndex = 0;
        }

        public override void Reset()
        {
            base.Reset();
            currentIndex = -1;
        }

        public override void Update()
        {
            Value = new float[] { InputController.STATE_NONE };
            if (Active && currentIndex != -1)
            {

                Inputs[currentIndex].Update();

                if(Inputs[currentIndex].TimerState == InputController.STATE_PRESSED)
                {
                    currentIndex++;
                    if(currentIndex == Inputs.Count)
                    {
                        Value[0] = InputController.STATE_RELEASED;
                    }
                } else if (Inputs[currentIndex].TimerState == InputController.STATE_RELEASED)
                {
                    Value[0] = InputController.STATE_RELEASED;
                }

                Timer.Update();
                //do logic here. Timer should reset the value if out of range.

                bool missTiming = false;

                if (Timer.IsSet)
                {
                    if (!Timer.InTime)
                    {
                        missTiming = true;
                    }
                }

                if ((missTiming && Value[0] == InputController.STATE_RELEASED) || Value[0] == InputController.STATE_RELEASED)
                {
                    SimulateRelease();
                }
            }
        }

    }
}
