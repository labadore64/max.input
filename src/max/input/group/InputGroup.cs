﻿using max.input.container;
using max.input.handler;
using max.input.handler.keyboard;
using max.serialize;
using System.Collections.Generic;
using System;

namespace max.input.group
{
    public class InputGroup : IMaxSerializable
    {
        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public bool Serializable { get; set; } = true;

        const string SER_HANDLER = "handler";

        public InputController Parent { get; private set; }

        List<IInputHandler> InputHandlers = new List<IInputHandler>();

        public bool Active { get; set; } = false;

        #region Constructors
        /// <summary>
        /// Empty Constructor.
        /// </summary>
        public InputGroup()
        {

        }

        public InputGroup(InputController Parent)
        {
            this.Parent = Parent;
        }

        public InputGroup(InputController Parent, MaxSerializedObject Data)
        {
            this.Parent = Parent;
            Deserialize(Data);
        }

        #endregion

        public void Update()
        {
            if (Active) {
                if (KeyboardHandler.StandardInput)
                {
                    //if standard input is active, update the standard input.
                    //if its active the regular keyboard handlers will not trigger.
                    KeyboardHandler.stdin.Update();
                }

                for (int i = 0; i < InputHandlers.Count; i++)
                {
                    InputHandlers[i].Update();
                }
            }
        }

        public void Reset()
        {

            for (int i = 0; i < InputHandlers.Count; i++)
            {
                InputHandlers[i].Reset();
            }
        }

        public void Enable()
        {
            Active = true;
        }

        public void Disable()
        {
            Active = false;
        }

        public IInputHandler AddHandler(IInputHandler Handler)
        {
            InputHandlers.Add(Handler);
            return Handler;
        }

        public void RemoveHandler(IInputHandler Handler)
        {
            InputHandlers.Remove(Handler);
        }

        public void RemoveHandler(Type Type)
        {
            for(int i = 0; i < InputHandlers.Count; i++)
            {
                if (InputHandlers[i].GetType() == Type)
                {
                    InputHandlers.RemoveAt(i);
                    i--;
                }
            }
        }

        public IInputHandler GetHandler(Type Type)
        {
            for (int i = 0; i < InputHandlers.Count; i++)
            {
                if (InputHandlers[i].GetType() == Type)
                {
                    return InputHandlers[i];
                }
            }
            return null;
        }

        public KeyboardHandler GetKeyboardHandler()
        {
            return (KeyboardHandler)GetHandler(typeof(KeyboardHandler));
        }

        public KeyboardHandler AddKeyboardHandler()
        {
            KeyboardHandler returner = GetKeyboardHandler();
            if (returner == null)
            {
                returner = new KeyboardHandler(this);
                AddHandler(returner);
                return returner;
            } else
            {
                return returner;
            }
        }

        #region Input Checking Methods

        public float GetInputState(string ID)
        {
            float returnValue = InputController.STATE_NONE;
            for (int i = 0; i < InputHandlers.Count; i++)
            {
                returnValue = InputHandlers[i].GetInputState(ID);
                if (returnValue != InputController.STATE_NONE)
                {
                    return returnValue;
                }
            }

            return returnValue;
        }

        public float GetBaseInputState(string ID)
        {
            float returnValue = InputController.STATE_NONE;
            for (int i = 0; i < InputHandlers.Count; i++)
            {
                returnValue = InputHandlers[i].GetBaseInputState(ID);
                if (returnValue != InputController.STATE_NONE)
                {
                    return returnValue;
                }
            }

            return returnValue;
        }

        public bool GetInputPressed(string ID)
        {
            if (GetInputState(ID) == InputController.STATE_PRESSED)
            {
                return true;
            }
            return false;
        }

        public bool GetInputHold(string ID)
        {
            if (GetInputState(ID) == InputController.STATE_HOLD)
            {
                return true;
            }
            return false;
        }

        public bool GetInputReleased(string ID)
        {
            if (GetInputState(ID) == InputController.STATE_RELEASED)
            {
                return true;
            }
            return false;
        }

        public bool GetInputNone(string ID)
        {
            if (GetInputState(ID) == InputController.STATE_NONE)
            {
                return true;
            }
            return false;
        }

        #endregion

        public IInputContainer GetInputContainer(Type Type, string ID)
        {
            IInputHandler handler = GetHandler(Type);
            if(handler != null)
            {
                return handler.GetInputContainer(ID);
            }
            return null;
        }

        public void ResetInput(string ID)
        {
            for(int i = 0; i < InputHandlers.Count; i++)
            {
                InputHandlers[i].ResetInput(ID);
            }
        }

        public void ResetInput(string[] IDs)
        {
            for (int i = 0; i < InputHandlers.Count; i++)
            {
                InputHandlers[i].ResetInput(IDs);
            }
        }

        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            List<MaxSerializedObject> handlers = new List<MaxSerializedObject>();
            for(int i = 0; i < InputHandlers.Count; i++)
            {
                handlers.Add(InputHandlers[i].Serialize());
            }
            Data.AddSerializedObjectCollection(SER_HANDLER, handlers);
            return Data;
        }

        public void Deserialize(MaxSerializedObject Data)
        {
            InputHandlers.Clear();
            IInputHandler Handler;
            if (Data.ContainsKey(SER_HANDLER))
            {
                if (Data[SER_HANDLER].GetType() == typeof(object[]))
                {
                    // adds the handlers as a list of serialized objects
                    List<MaxSerializedObject> handlers = Data.GetSerializedObjectCollection(SER_HANDLER);

                    for (int i = 0; i < handlers.Count; i++)
                    {
                        Handler = (IInputHandler)handlers[i].Deserialize();
                        Handler.ParentGroup = this;
                        InputHandlers.Add(Handler);
                    }
                } else
                {
                    // adds the object as the only handler.
                    Handler = (IInputHandler)Data.GetSerializedObject(SER_HANDLER).Deserialize();
                    Handler.ParentGroup = this;
                    InputHandlers.Add(Handler);
                }
            }
        }
        public IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new InputGroup();
            a.Deserialize(Data);
            return a;
        }
    }
}
